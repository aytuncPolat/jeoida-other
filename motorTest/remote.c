/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "crc16.h"
#include "joystick.h"
#include <termios.h>


void structInitializer();
int sendSerialPort(int fd);
int openSerialPort(void);
int calcCheckSum(char* buffer, int len);

#define packSize 17

union __attribute__((packed)) rs485Union
{
	struct __attribute__((packed)) rs485Struct
	{
		char start [3];
		uint8_t devAddress;
		uint8_t cmd;
		uint8_t regAddress;
		float data;
		int csum;
		char end[3];
	}fields;

	uint8_t buffer[packSize];
}txUnion;


void sendIMCPacket(void);

int sock, n;
unsigned int length;
struct sockaddr_in server, from;
struct hostent *hp;
char buffer[256];
int sendCounter = 0;


char imcBuffer[27] = {0x54 ,0xfe ,0x2d ,0x01 ,0x05 ,0x00 ,0x4c ,0x37 ,0xc1 ,0xa9 ,0xc6 ,0x5d ,0xd6 ,0x41 ,0x00 ,0x00 ,0x00 ,0xff ,0xff ,0xff ,0x01 ,0xcd ,0xcc ,0x4c ,0x3e ,0x08 ,0xfb};
//char imcBuffer[27] = {0x54 ,0xfe ,0x2d ,0x01 ,0x05 ,0x00 ,0x4c ,0x37 ,0xc1 ,0xa9 ,0xc6 ,0x5d ,0xd6 ,0x41 ,0x00 ,0x00 ,0x00 ,0xff ,0xff ,0xff ,0x01 ,0xcd ,0xcc ,0x4c ,0x3e, 0x00, 0x00};
uint16_t crcVal;

struct joyA
{
	float triger_R;
	float triger_L;
}joyAxis;

struct joyB
{
	uint8_t rB;
	uint8_t lB;
}joyButton;

struct stck
{
	float r_x;
	float r_y;
	float l_x;
	float l_y;
}stick;

struct abxyBtn
{
	uint8_t a;
	uint8_t b;
	uint8_t x;
	uint8_t y;
}abxyButtons;


struct ctrl
{
	float motorR;
	float motorL;
	float irgat;		//:D:D
}control;


int state = 0;

union __attribute__((packed)) shortData
{
	uint16_t val;
	char buffer[2];
}shortVal;

union __attribute__((packed)) payload
{
	float val;
	char buffer[4];
}floatVal;


void error(const char *);

int main(int argc, char *argv[])
{
	int fd, rc, rs422;
	int done = 0;
	struct js_event jse;
	fd = open_joystick();
	if (fd < 0)
	{
		printf("open failed.\n");
		exit(1);
	}

	rs422 = openSerialPort();

	structInitializer();

	while (1)
	{
		rc = read_joystick_event(&jse);
		usleep(1000);
		if (1)//(rc == 1)
		{
			//printf("Event: time %8u, value %8hd, type: %3u, axis/button: %u\n" ,jse.time, jse.value, jse.type, jse.number);

			if(jse.type == 1 && jse.number == 3)
			{
				abxyButtons.y = (uint8_t) jse.value;
			}

			if(jse.type == 1 && jse.number == 1)
			{
				abxyButtons.b = (uint8_t) jse.value;
			}

			if(jse.type == 1 && jse.number == 2)
			{
				abxyButtons.x = (uint8_t) jse.value;
			}

			if(jse.type == 1 && jse.number == 0)
			{
				abxyButtons.a = (uint8_t) jse.value;
			}

			if(jse.type == 2 && jse.number == 1)
			{
				stick.l_x = (jse.value/32768.0)*-1.2;
				//printf("stick.l_x = %f", stick.l_x);
			}

			if(jse.type == 2 && jse.number == 0)
			{
				stick.l_y = (jse.value/32768.0)*1.2;
				//printf("stick.l_y = %f", stick.l_y);
			}

			if(jse.type == 2 && jse.number == 5)
			{
				joyAxis.triger_R = ((jse.value + 32768)/65535.0)*1.2;
				//printf("triger_R = %f", joyAxis.triger_R);
			}
			if(jse.type == 2 && jse.number == 2)
			{
				joyAxis.triger_L = ((jse.value + 32768)/65535.0)*1.2;
				//printf("triger_L = %f", joyAxis.triger_L);
			}

			if(jse.type == 1 && jse.number == 5)
			{
				joyButton.rB = jse.value;
			}

			if(jse.type == 1 && jse.number == 4)
			{
				joyButton.lB = jse.value;
			}

			control.motorR = stick.l_x + stick.l_y;
			control.motorL = stick.l_x - stick.l_y;

			if(sendCounter++ > 10)
			{
				printf("%f \n\r",control.motorR*1000);
				sendCounter = 0;
				txUnion.fields.data = control.motorR*1000;
				txUnion.fields.csum = calcCheckSum(txUnion.buffer, packSize);
				sendSerialPort(rs422);
			}

			//triger control
			/*
			if(joyButton.rB)control.motorR = joyAxis.triger_R;
			else control.motorR = -joyAxis.triger_R;

			if(joyButton.lB)control.motorL = joyAxis.triger_L;
			else control.motorL = -joyAxis.triger_L;
			*/
		}
	}
	close(sock);
	return 0;
}

void error(const char *msg)
{
	perror(msg);
	exit(0);
}

int openSerialPort(void)
{
	int fd; /* File descriptor for the port */
	int n;
	struct termios tio;
	memset(&tio,0,sizeof(tio));
	tio.c_iflag=0;
	tio.c_oflag=0;
	tio.c_cflag=CS8|CREAD|CLOCAL;           // 8n1, see termios.h for more information
	tio.c_lflag=0;
	tio.c_cc[VMIN]=1;
	tio.c_cc[VTIME]=5;
	cfsetospeed(&tio,B115200);            // 115200 baud
	cfsetispeed(&tio,B115200);            // 115200 baud
	fd = open("/dev/ttyUSB0");

	if (fd == -1) perror("open_port: Unable to open /dev/ttyUSB0 - ");
	else fcntl(fd, 0);
	return (fd);
}

int sendSerialPort(int fd)
{
	int error = 0;
	const char *ch = &txUnion.buffer[0];
	error = write(fd, ch, 17);
	if (error < 0) fputs("ERROR!!\n", stderr);
}

void structInitializer()
{
	txUnion.fields.start[0] = '$';
	txUnion.fields.start[1] = ':';
	txUnion.fields.start[2] = '#';
	txUnion.fields.devAddress = 0;
	txUnion.fields.cmd = 2;
	txUnion.fields.regAddress = 5;

	txUnion.fields.end[0] = '#';
	txUnion.fields.end[1] = ':';
	txUnion.fields.end[2] = '/';
}

int calcCheckSum(char* buffer, int len)
{
	int i = 0;
	int sum = 0;
	for(i = 0 ; i < len-7 ; i++)
	{
		uint8_t temp = *(buffer+i);
		sum += temp;
		//printf("sum = %u  ch = %u \r\n",sum, ((uint8_t)*(buffer+i)));
	}
	return sum;
}
