/* UDP client in the internet domain */
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "crc16.h"
#include "joystick.h"

void sendIMCPacket(void);

int sock, n;
unsigned int length;
struct sockaddr_in server, from;
struct hostent *hp;
char buffer[256];


char imcBuffer[27] = {0x54 ,0xfe ,0x2d ,0x01 ,0x05 ,0x00 ,0x4c ,0x37 ,0xc1 ,0xa9 ,0xc6 ,0x5d ,0xd6 ,0x41 ,0x00 ,0x00 ,0x00 ,0xff ,0xff ,0xff ,0x01 ,0xcd ,0xcc ,0x4c ,0x3e ,0x08 ,0xfb};
//char imcBuffer[27] = {0x54 ,0xfe ,0x2d ,0x01 ,0x05 ,0x00 ,0x4c ,0x37 ,0xc1 ,0xa9 ,0xc6 ,0x5d ,0xd6 ,0x41 ,0x00 ,0x00 ,0x00 ,0xff ,0xff ,0xff ,0x01 ,0xcd ,0xcc ,0x4c ,0x3e, 0x00, 0x00};
uint16_t crcVal;

struct joyA
{
	float triger_R;
	float triger_L;
}joyAxis;

struct joyB
{
	uint8_t rB;
	uint8_t lB;
}joyButton;

struct stck
{
	float r_x;
	float r_y;
	float l_x;
	float l_y;
}stick;

struct abxyBtn
{
	uint8_t a;
	uint8_t b;
	uint8_t x;
	uint8_t y;
}abxyButtons;


struct ctrl
{
	float motorR;
	float motorL;
	float irgat;		//:D:D
}control;


int state = 0;

union __attribute__((packed)) shortData
{
	uint16_t val;
	char buffer[2];
}shortVal;

union __attribute__((packed)) payload
{
	float val;
	char buffer[4];
}floatVal;


void error(const char *);
int main(int argc, char *argv[])
{
	int fd, rc;
	int done = 0;
	struct js_event jse;
	fd = open_joystick();
	if (fd < 0)
	{
		printf("open failed.\n");
		exit(1);
	}



	if (argc != 3)
	{
		printf("Usage: server port\n");
		exit(1);
	}
	sock= socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) error("socket");

	server.sin_family = AF_INET;
	hp = gethostbyname(argv[1]);
	if (hp==0) error("Unknown host");

	bcopy((char *)hp->h_addr,(char *)&server.sin_addr,hp->h_length);
	server.sin_port = htons(atoi(argv[2]));
	length=sizeof(struct sockaddr_in);

	while (1)
	{
		rc = read_joystick_event(&jse);
		usleep(1000);
		if (rc == 1)
		{
			printf("Event: time %8u, value %8hd, type: %3u, axis/button: %u\n" ,jse.time, jse.value, jse.type, jse.number);

			if(jse.type == 1 && jse.number == 3)
			{
				abxyButtons.y = (uint8_t) jse.value;
			}

			if(jse.type == 1 && jse.number == 1)
			{
				abxyButtons.b = (uint8_t) jse.value;
			}

			if(jse.type == 1 && jse.number == 2)
			{
				abxyButtons.x = (uint8_t) jse.value;
			}

			if(jse.type == 1 && jse.number == 0)
			{
				abxyButtons.a = (uint8_t) jse.value;
			}

			if(jse.type == 2 && jse.number == 1)
			{
				stick.l_x = (jse.value/32768.0)*-1.2;
				printf("stick.l_x = %f", stick.l_x);
			}

			if(jse.type == 2 && jse.number == 0)
			{
				stick.l_y = (jse.value/32768.0)*1.2;
				printf("stick.l_y = %f", stick.l_y);
			}

			if(jse.type == 2 && jse.number == 5)
			{
				joyAxis.triger_R = ((jse.value + 32768)/65535.0)*1.2;
				printf("triger_R = %f", joyAxis.triger_R);
			}
			if(jse.type == 2 && jse.number == 2)
			{
				joyAxis.triger_L = ((jse.value + 32768)/65535.0)*1.2;
				printf("triger_L = %f", joyAxis.triger_L);
			}

			if(jse.type == 1 && jse.number == 5)
			{
				joyButton.rB = jse.value;
			}

			if(jse.type == 1 && jse.number == 4)
			{
				joyButton.lB = jse.value;
			}

			control.motorR = stick.l_x + stick.l_y;
			control.motorL = stick.l_x - stick.l_y;







			//triger control
			/*
			if(joyButton.rB)control.motorR = joyAxis.triger_R;
			else control.motorR = -joyAxis.triger_R;

			if(joyButton.lB)control.motorL = joyAxis.triger_L;
			else control.motorL = -joyAxis.triger_L;
			*/


			imcBuffer[2] = 0x2d;
			imcBuffer[3] = 0x01;

			if(state == 0)
			{
				state = 1;
				imcBuffer[20] = 0;
				floatVal.val = control.motorR;
				sendIMCPacket();
			}

			if(state == 1)
			{
				state = 0;
				imcBuffer[20] = 1;
				floatVal.val = control.motorL;
				sendIMCPacket();
			}
			imcBuffer[2] = 0x2e;
			imcBuffer[3] = 0x01;
			/////////////////////
			imcBuffer[20] = 0;
			floatVal.val = 1.0*abxyButtons.a;
			sendIMCPacket();

			if(abxyButtons.b == 1)
			{
				imcBuffer[20] = 1;
				floatVal.val = 0.5*abxyButtons.b;
			}
			else
			{
				imcBuffer[20] = 1;
				floatVal.val = 0;

				if(abxyButtons.y == 1)
				{
					imcBuffer[20] = 1;
					floatVal.val = -0.5*abxyButtons.y;
				}
				else
				{
					imcBuffer[20] = 1;
					floatVal.val = 0;
				}
			}
			sendIMCPacket();

			imcBuffer[20] = 3;
			floatVal.val = 1.0*abxyButtons.x;
			sendIMCPacket();


		}
	}
	close(sock);
	return 0;
}

void error(const char *msg)
{
	perror(msg);
	exit(0);
}

void sendIMCPacket(void)
{
	imcBuffer[21] = floatVal.buffer[0];
	imcBuffer[22] = floatVal.buffer[1];
	imcBuffer[23] = floatVal.buffer[2];
	imcBuffer[24] = floatVal.buffer[3];

	crcVal = crc_16(imcBuffer, 25);
	shortVal.val = crcVal;
	imcBuffer[25] = shortVal.buffer[0];
	imcBuffer[26] = shortVal.buffer[1];

	n=sendto(sock,imcBuffer,27,0,(const struct sockaddr *)&server,length);
	if (n < 0) error("Sendto");
}
