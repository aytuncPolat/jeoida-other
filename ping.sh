#!/bin/sh

ping -q -c5 10.0.99.2 > /dev/null
 
if [ $? -eq 0 ]
then
	echo "true"
else
	echo "false"
	cd /sys/class/gpio/ &&
	echo 60 > export &&
	cd gpio60 &&
	echo out > direction &&
	echo 1 > value
fi
